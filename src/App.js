import React, { Component, useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const API_URL = 'http://localhost:8080';

  const [allRecords, setAllRecords] = useState([]);
  const [record, setRecord] = useState({});

  useEffect(() => {
    fetch(API_URL).then(response => {
      response.json().then(result => {
        setAllRecords(result);
      })
    })
  }, [])

  function handleSubmit(event) {
    event.preventDefault();

    const config = {
      method: "POST",
      body: JSON.stringify(record),
      headers: {
        'Content-Type': 'application/json'
      },
    }

    fetch(API_URL, config)
      .then(response => response.json())
      .then(() => {
        fetch(API_URL).then(response => {
          response.json().then(result => {
            setAllRecords(result);
          })
        })
      });
  }

    return (
      <div className="App">
        <header className="App-header">
          <p>
            Demo App
          </p>
          <form onSubmit={handleSubmit} style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}>
            <label htmlFor="name">Enter Title: </label>
            <input
              id="title"
              type="text"
              onChange={(e) => setRecord({title: e.target.value, description: record.description})}
            />
            <br />
            <label htmlFor="name">Enter Description: </label>
            <input
              id="description"
              type="text"
              onChange={(e) => setRecord({description: e.target.value, title: record.title})}
            />
            <br />
            <button type="submit">Submit</button>
          </form>
          <hr />
          <table>
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {
                allRecords.map(x => {
                  return <>
                    <tr>
                      <td>{x.id}</td>
                      <td>{x.title}</td>
                      <td>{x.description}</td>
                    </tr>
                  </>
                })
              }
              <tr>
                <td>0e5d75ae-1da4-49f3-a8b1-a4acaefdce08</td>
                <td>hello</td>
                <td>world</td>
              </tr>
            </tbody>
          </table>
        </header>
      </div>
    );
}


export default App;
